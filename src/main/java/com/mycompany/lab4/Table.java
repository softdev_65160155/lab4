/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.lab4;

/**
 *
 * @author informatics
 */
public class Table {

    private char[][] table = {{'-', '-', '-'}, {'-', '-', '-'}, {'-', '-', '-'}};
    private Player currentplayer;
    private int row, col;
    private Player player1;
    private Player player2;
    private int turncount = 0;

    public Table(Player player1, Player player2) {
        this.player1 = player1;
        this.player2 = player2;
        this.currentplayer = player1;
    }
    public Player getCurrentplayer(){
       return currentplayer;
            }

    public char[][] getTable() {
        return table;
    }

   

    public boolean setrowcol(int row,int col) {
        if (table[row-1][col-1] != '-'){
            return false;
        }
        table[row-1][col-1] = currentplayer.getSymbol();
        this.row = row;
        this.col = col;
        this.turncount++;
        return true;
    }

    public boolean checkWin() {
        if (isWin()) {
            
            return true;
        }
        return false;
    }
    public boolean checkDraw(){
      if (isTie()) {
          
          return true;
        }
     return false;
    }

    public void switchplayer() {
        if (currentplayer == player1) {
           currentplayer = player2;
        }
         else {
           currentplayer = player1;
    }
    }
    public void resetplayer(){
       currentplayer =player1;
       player2 = player2;
       
    }

    public boolean isWin() {
        if (rowcheck(table)||colcheck(table)||diagonalcheck(table)||diagonalcheck2(table)){
            savewin();
            return true;
        }
        return false;
    }

    public boolean isTie() {
        if (checkboardfull(table)) {
             savedraw();
            return true;
        }
        return false;
    }

    private boolean rowcheck(char[][] table) {
        //this.table = table;
        //this.currentplayer = currentplayer;
                
         return table[row-1][0]!= '-' && table[row-1][0] == table[row-1][1] && table[row-1][2] == table[row-1][1];   
    }

    private boolean colcheck(char[][] table) {
        //this.table = table;
        //this.currentplayer = currentplayer;
     
        return table[0][col-1]!= '-' && table[0][col-1] == table[1][col-1] && table[2][col-1] == table[1][col-1];
    }

    private boolean diagonalcheck(char[][] table) {
        //this.table = table;
        //this.currentplayer = currentplayer;
        return table[0][0]!= '-'&& table[1][1]!= '-'&& table[2][2]!= '-' && table[0][0] ==  table[1][1] && table[1][1]==table[2][2] && table[2][2]==table[0][0];
    
        }
        
    
    private boolean diagonalcheck2(char[][] table){
         return table[0][2]!= '-'&& table[1][1]!= '-'&& table[2][0]!= '-' && table[2][0] == table[1][1] && table[0][2] ==table[1][1]&& table[2][0] ==table[0][2];
    }

    private boolean checkboardfull(char[][] table) {
        //this.table = table;
        for (int row = 0; row < 3; row++) {
            for (int col = 0; col < 3; col++) {
                if (table[row][col] == '-') {
                    return false;
                }
            }
        }
        return true;
    }
    private void  savewin(){
    if(player1== getCurrentplayer()){
        player1.getWin();
        player2.getLose();
    
    }
    if(player2== getCurrentplayer()){
        player2.getWin();
        player1.getLose();
    }
    }
    private void  savedraw(){
    
        player1.getDraw();
        player2.getDraw();
   
  
    
    }

}
