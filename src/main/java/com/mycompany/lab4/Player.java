/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.lab4;

/**
 *
 * @author informatics
 */
public class Player {
 private char symbol;
 private int wincount,losecount,drawcount;

    Player(char symbol) {
      this.symbol = symbol;        
    }

    public int getWincount() {
        return wincount;
    }

    public int getLosecount() {
        return losecount;
    }

    public int getDrawcount() {
        return drawcount;
    }

    public char getSymbol() {
        return symbol;
    }
    public int getWin(){
    return wincount++;
    }
    public int getLose(){
    return losecount++;
    }
    public int getDraw(){
    return drawcount++;
    }

    @Override
    public String toString() {
        return "Player{" + "symbol=" + symbol + ", wincount=" + wincount + ", losecount=" + losecount + ", drawcount=" + drawcount + '}';
    }
    
}
