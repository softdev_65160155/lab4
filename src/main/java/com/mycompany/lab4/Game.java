package com.mycompany.lab4;

import java.util.Scanner;

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
/**
 *
 * @author informatics
 */
public class Game {

    private Table table;
    private Player player1;
    private Player player2;

    public Game() {
        player1 = new Player('X');
        player2 = new Player('O');
    }

    public void playGame() {
        Scanner kb = new Scanner(System.in);
        String Continue;
        showWelcome();
        newgame();
        while (true) {
            showTable();
            showTurn();
            inputtrowcol();
            if (table.checkWin()) {

                System.out.println(table.getCurrentplayer() + " is a winner!");
                System.out.println("Result");
                showTable();
                showinfo();
                System.out.println("Please input continue or exit: ");
                Continue = kb.nextLine();
                if (Continue.equals("continue")) {
                    newgame();
                    table.switchplayer();

                }
                if (Continue.equals("exit")) {
                    break;
                }
            }
            if (table.checkDraw()) {
                table.switchplayer();
                System.out.println("Tie No one win");
                System.out.println("Result");
                showTable();
                showinfo();
                System.out.println("Please input continue or exit: ");
                Continue = kb.nextLine();
                if (Continue.equals("continue")) {
                   
                    newgame();
                    table.switchplayer();

                }
                if (Continue.equals("exit")) {
                    break;
                }

            }
            table.switchplayer();
        }
    }

    public void showWelcome() {
        System.out.println("Welcome to OX");
    }

    public void showTable() {
        char[][] ntable = table.getTable();
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                System.out.print(ntable[i][j] + " ");
            }
            System.out.println("");
        }

    }

    public void showTurn() {
        char current = table.getCurrentplayer().getSymbol();
        System.out.println(current + " Turn");
    }

    private void inputtrowcol() {
        Scanner kb = new Scanner(System.in);
        int row;
        int col;

        while (true) {
            System.out.print("Please input row,col:");
            row = kb.nextInt();
            col = kb.nextInt();
            if (table.setrowcol(row, col) == false) {
                System.out.println("Invalid move");
                continue;
            }
            table.setrowcol(row, col);
            break;
        }

    }

    private void newgame() {
        table = new Table(player1, player2);

    }

    private void showinfo() {
        System.out.println(player1);
        System.out.println(player2);
    }

}

// private boolean checkwin(char[][] tables, Player currentplayer) {
//    boolean t = table.checkWin(tables,currentplayer);
//    if(t){
//         return true;
//    }
//    return false;
// }

